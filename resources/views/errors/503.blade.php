@extends('layouts.errors')

{{-- PAGE TITLE --}}
@section('title')
We'll Be Right Back!
@stop

{{-- CONTENT --}}
@section('content')
<div class="container-xs-height full-height">
    <div class="row-xs-height">
        <div class="col-xs-height col-middle">
            <div class="error-container text-center">
                <h1 class="error-number">503</h1>

                <h2 class="semi-bold">Don't Panic, Please Stand By!</h2>

                <p>
                    We're currently down for scheduled maintenance and will be back shortly.
                    We apologize for any inconvenience!
                </p>
            </div>
        </div>
    </div>
</div>
@stop