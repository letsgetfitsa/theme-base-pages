<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
  <meta charset="utf-8" />
  <title>
    @section('title')
    @show
  </title>

  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

  <link rel="apple-touch-icon" href="{{ getUrl('pages/ico/60.png') }}">
  <link rel="apple-touch-icon" sizes="76x76" href="{{ getUrl('pages/ico/76.png') }}">
  <link rel="apple-touch-icon" sizes="120x120" href="{{ getUrl('pages/ico/120.png') }}">
  <link rel="apple-touch-icon" sizes="152x152" href="{{ getUrl('pages/ico/152.png') }}">
  <link rel="icon" type="image/x-icon" href="{{ getUrl('favicon.ico') }}" />
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-touch-fullscreen" content="yes">
  <meta name="apple-mobile-web-app-status-bar-style" content="default">

  <meta name="description" content="" />
  <meta name="author" content=""/>

  {{ asset_queue('pace-theme-flash', 'plugins/pace/pace-theme-flash.css') }}
  {{ asset_queue('bootstrap', 'plugins/boostrapv3/css/bootstrap.min.css') }}
  {{ asset_queue('font-awesome', 'plugins/font-awesome/css/font-awesome.css') }}
  {{ asset_queue('jquery.scrollbar', 'plugins/jquery-scrollbar/jquery.scrollbar.css') }}
  {{ asset_queue('select2', 'plugins/bootstrap-select2/select2.css') }}
  {{ asset_queue('switchery.min', 'plugins/switchery/css/switchery.min.css') }}
  {{ asset_queue('pages-icons', 'pages/css/pages-icons.css') }}
  {{ asset_queue('pages', 'pages/css/pages.css') }}

  {{ getCompiledStyles() }}

  @section('styles')
  @show

  <!--[if lte IE 9]>
  <link href="{{ getUrl('pages/css/ie9.css') }}" rel="stylesheet" type="text/css" />
  <![endif]-->

  <script type="text/javascript">
    window.onload = function()
    {
      // fix for windows 8
      if (navigator.appVersion.indexOf("Windows NT 6.2") != -1)
        document.head.innerHTML += '<link rel="stylesheet" type="text/css" href="{{ getUrl('pages/css/windows.chrome.fix.css') }}" />'
    }
  </script>
</head>

<body class="fixed-header ">

{{-- SIDEBAR/NAV --}}
@include('layouts.partials.sidebar-left')

<!-- START PAGE-CONTAINER -->
<div class="page-container">

  {{-- PAGE HEADER --}}
  @include('layouts.page-header')

  {{-- PAGE CONTENT --}}
  @include('layouts.page-content')

</div>
<!-- END PAGE CONTAINER -->

{{-- RIGHT SIDEBAR QUICKVIEW --}}
@include('layouts.partials.quickview')

{{-- SEARCH MODAL OVERLAY --}
@include('layouts.partials.search-overlay')

<!-- BEGIN VENDOR JS -->
{{ asset_queue('pace.min', 'plugins/pace/pace.min.js') }}
{{ asset_queue('jquery', 'plugins/jquery/jquery-1.11.1.min.js') }}
{{ asset_queue('modernizr', 'plugins/modernizr.custom.js') }}
{{ asset_queue('jquery-ui', 'plugins/jquery-ui/jquery-ui.min.js', ['jquery']) }}
{{ asset_queue('bootstrap', 'plugins/boostrapv3/js/bootstrap.min.js', ['jquery']) }}
{{ asset_queue('jquery.easy', 'plugins/jquery/jquery-easy.js', ['jquery']) }}
{{ asset_queue('jquery.unveil', 'plugins/jquery-unveil/jquery.unveil.min.js', ['jquery']) }}
{{ asset_queue('jquery.bez', 'plugins/jquery-bez/jquery.bez.min.js', ['jquery']) }}
{{ asset_queue('jquery.ioslist', 'plugins/jquery-ios-list/jquery.ioslist.min.js', ['jquery']) }}
{{ asset_queue('imagesloaded', 'plugins/imagesloaded/imagesloaded.pkgd.min.js') }}
{{ asset_queue('jquery.actual', 'plugins/jquery-actual/jquery.actual.min.js', ['jquery']) }}
{{ asset_queue('jquery.scrollbar', 'plugins/jquery-scrollbar/jquery.scrollbar.min.js', ['jquery']) }}
{{ asset_queue('pages', 'pages/js/pages.min.js', ['jquery', 'bootstrap']) }}
{{ asset_queue('scripts', 'js/scripts.js', ['pages']) }}

{{ getCompiledScripts() }}

@section('styles')
@show
</body>
</html>