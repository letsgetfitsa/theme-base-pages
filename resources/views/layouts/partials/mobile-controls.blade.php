<!-- START MOBILE CONTROLS -->
<!-- LEFT SIDE -->
<div class="pull-left full-height visible-sm visible-xs">
    <div class="sm-action-bar">
        <a href="#" class="btn-link toggle-sidebar" data-toggle="sidebar">
            <span class="icon-set menu-hambuger"></span>
        </a>
    </div>
</div>

<!-- RIGHT SIDE -->
<div class="pull-right full-height visible-sm visible-xs">
    <div class="sm-action-bar">
        <a href="#" class="btn-link" data-toggle="quickview" data-toggle-element="#quickview">
            <span class="icon-set menu-hambuger-plus"></span>
        </a>
    </div>

</div>
<!-- END MOBILE CONTROLS -->