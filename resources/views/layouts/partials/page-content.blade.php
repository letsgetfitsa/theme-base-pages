<!-- START PAGE CONTENT WRAPPER -->
<div class="page-content-wrapper">

    <!-- START PAGE CONTENT -->
    <div class="content">

        <!-- START JUMBOTRON -->
        <div class="jumbotron no-margin" data-pages="parallax">
            <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
                <div class="inner">
                    <h3 class="">
                        @section('pageHeader')
                        @show
                    </h3>
                </div>
            </div>
        </div>

        <div class="container-fluid container-fixed-lg">
            <!-- START BREADCRUMB -->
            <ul class="breadcrumb">
                @section('breadcrumb')
                <li>
                    <p>Home</p>
                </li>
                <li>
                    <a href="#" class="active">Parallax for page title</a>
                </li>
                @show
            </ul><!-- END BREADCRUMB -->
        </div>

        {{-- PAGE CONTENT --}}
        @yield('content')
    </div>

    <!-- START FOOTER -->
    <div class="container-fluid container-fixed-lg footer">

        <div class="copyright sm-text-center">

            <p class="small no-margin pull-left sm-pull-reset">
                <span class="hint-text">Copyright © {{ date('Y') }}</span>
                <span class="font-montserrat">REVOX</span>.
                <span class="hint-text">All rights reserved.</span>
              <span class="sm-block">
                <a href="{{ url('pages/terms') }}" class="m-l-10 m-r-10">Terms of use</a> | <a href="{{ url('pages/privacy') }}" class="m-l-10">Privacy Policy</a>
              </span>
            </p>

            <p class="small no-margin pull-right sm-pull-reset">
                <span class="hint-text">Hand-crafted &amp; Made with Love by <a href="http://shanedanielsagency.com">Shane Daniels</a> ®</span>
            </p>

            <div class="clearfix"></div>
        </div>
    </div>
    <!-- END FOOTER -->
</div>
<!-- END PAGE CONTENT WRAPPER -->