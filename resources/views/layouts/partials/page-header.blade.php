<!-- START PAGE HEADER WRAPPER -->
<!-- START HEADER -->
<div class="header ">

    {{-- MOBILE CONTROLS --}}
    @include('layouts.partials.mobile-controls')

    <div class=" pull-left sm-table">
        <div class="header-inner">

            <div class="brand inline">
                <img src="{{ getUrl('img/logo.png') }}" alt="logo" data-src="{{ getUrl('img/logo.png') }}" data-src-retina="{{ getUrl('img/logo_2x.png') }}" width="93" height="25">
            </div>

            {{-- PAGE HEADER NOTIFICATIONS DROPDOWN --}}
            @include('layouts.partials.page-header.notifications')

        </div>
    </div>
    <div class=" pull-right">
        <div class="header-inner">
            <a href="#" class="btn-link icon-set menu-hambuger-plus m-l-20 sm-no-margin hidden-sm hidden-xs" data-toggle="quickview" data-toggle-element="#quickview"></a>
        </div>
    </div>

    <div class=" pull-right">

        <!-- START User Info-->
        <div class="visible-lg visible-md m-t-10">
            <div class="pull-left p-r-10 p-t-10 fs-16 font-heading">
                <span class="semi-bold">David</span>
                <span class="text-master">Nest</span>
            </div>
            <div class="thumbnail-wrapper d32 circular inline m-t-5">
                <img src="assets/img/profiles/avatar.jpg" alt="" data-src="assets/img/profiles/avatar.jpg" data-src-retina="assets/img/profiles/avatar_small2x.jpg" width="32" height="32">
            </div>
        </div>
        <!-- END User Info-->

    </div>
</div>
<!-- END HEADER -->
<!-- END PAGE HEADER WRAPPER -->